(function($){
  /**
   * Default setup
   */
  function PreventImgLoad() {
    this.regional = []; // Available regional settings, indexed by language code
  	this._defaults = {};
  	$.extend(this._defaults, this.regional['']);
  }
  
  $.extend(PreventImgLoad.prototype, {
    /**
     * Class name added to elements to indicate already configured by this plugin.
     */
	  markerClassName: 'image-processing',
    /**
     * Name of the data property for instance settings.
     */
	  propertyName: 'preventImgLoad',
    /**
     * Name of the data property for src value.
     */
	  dataName: 'preventImgSrc',
	  /**
	   * Attach the plugin functionality.
	   * @param  target   (element) the control to affect
	   * @param  options  (object) the custom options for this instance
	   */
    _attachPlugin: function(target, options) {
  		target = $(target);
  		if (target.hasClass(this.markerClassName)) {
  			return;
  		}
  		var inst = {options: $.extend({}, this._defaults)};
  		target.addClass(this.markerClassName).data(this.propertyName, inst);
  		$(window).bind('resize.' + this.propertyName, target, plugin._handleImage);
  		$(document).bind('ready.' + this.propertyName, target, plugin._handleImage);
  	},
  	/**
	   * Handle image display
	   * @param  evt   (object) The event opject
	   */
  	_handleImage: function(evt) {
  	  var target = evt.data;
      if(target.not(':visible').size() > 0 && target.attr('src') != '') {
        target.data(plugin.dataName, target.attr('src'));
        target.attr('src', '');
      } else if(target.is(':visible') && target.attr('src') == '') {
        target.attr('src', target.data(plugin.dataName));
      }
    },
  });
  /**
   * Singleton instance
   */
  var plugin = $.preventimgload = new PreventImgLoad();
  
  $.fn.preventimgload = function(options){
    return this.each(function(){
      plugin._attachPlugin(this, options || {});
    });
  }
}(jQuery));